const { test, expect } = require('@playwright/test');
const { LoginPage } = require ('../page/login.page');
const { TopBar } = require ('../page/top_bar.page');
const { FoldersPage } = require ('../page/folders.page');

test.beforeEach(async ({ page }) => {
  const loginPage = new LoginPage(page);
  await loginPage.open();
  await loginPage.login(process.env.USERNAME, process.env.PASSWORD);
});

test.afterEach(async ({ page }) => {
  const topBar = new TopBar(page);
  await topBar.openUserdropdown();
  await topBar.logout();
});

test('User can create parent folder', async ({ page }) => {
  const foldersPage = new FoldersPage(page);
  await foldersPage.open();
  await foldersPage.addFolder('Bills');
  await foldersPage.notificationShows('Bills created');
  await foldersPage.parentFolderHasBeenAdded('Bills', 1, 'checked');
  await foldersPage.deleteParentFolder('Bills');
  await foldersPage.notificationShows('Bills removed');
  await foldersPage.folderHasBeenRemoved('Bills');
});

test('User can create sub folder', async ({ page }) => {
  const foldersPage = new FoldersPage(page);
  await foldersPage.open();
  await foldersPage.addFolder('Bills2');
  await foldersPage.notificationShows('Bills2 created')
  await foldersPage.parentFolderHasBeenAdded('Bills2', 1, 'checked');
  await foldersPage.addSubFolder('Electricity', 'Bills2');
  await foldersPage.notificationShows('Electricity created');
  await foldersPage.subFolderHasBeenAdded('Electricity', 'Bills2', 1, 'on');
  await foldersPage.deleteSubFolder('Electricity', 'Bills2');
  await foldersPage.notificationShows('Electricity removed');
  await foldersPage.subfolderHasBeenRemoved('Electricity', 'Bills2');
  await foldersPage.deleteParentFolder('Bills2');
  await foldersPage.notificationShows('Bills2 removed');
  await foldersPage.folderHasBeenRemoved('Bills2');
});

test("User can't create folder with already taken name", async ({ page }) => {
  const foldersPage = new FoldersPage(page);
  await foldersPage.open();
  await foldersPage.addFolder('Bills3');
  await foldersPage.notificationShows('Bills3 created');
  await foldersPage.parentFolderHasBeenAdded('Bills3', 1, 'checked');
  await foldersPage.addFolder('Bills3');
  await foldersPage.notificationShows('A label or folder with this name already exists')
  await foldersPage.cancelFolderCreation();
  await foldersPage.deleteParentFolder('Bills3');
  await foldersPage.notificationShows('Bills3 removed');
  await foldersPage.folderHasBeenRemoved('Bills3');
});

test("User can't create subfolder with already taken name", async ({ page }) => {
  const foldersPage = new FoldersPage(page);
  await foldersPage.open();
  await foldersPage.addFolder('Bills4');
  await foldersPage.notificationShows('Bills4 created');
  await foldersPage.parentFolderHasBeenAdded('Bills4', 1, 'checked');
  await foldersPage.addSubFolder('Electricity', 'Bills4');
  await foldersPage.notificationShows('Electricity created');
  await foldersPage.subFolderHasBeenAdded('Electricity', 'Bills4', 1, 'on');
  await foldersPage.addSubFolder('Electricity', 'Bills4');
  await foldersPage.notificationShows('A sub-folder with this name already exists in the destination folder')
  await foldersPage.cancelFolderCreation();
  await foldersPage.deleteParentFolder('Bills4');
  await foldersPage.notificationShows('Bills4 removed');
  await foldersPage.folderHasBeenRemoved('Bills4');
});

test("User can't create more than 3 folders", async ({ page }) => {
  const foldersPage = new FoldersPage(page);
  await foldersPage.open();
  await foldersPage.addFolder('Bills5');
  await foldersPage.notificationShows('Bills5 created');
  await foldersPage.parentFolderHasBeenAdded('Bills5', 1, 'checked');
  await foldersPage.addFolder('Bills6');
  await foldersPage.notificationShows('Bills6 created');
  await foldersPage.parentFolderHasBeenAdded('Bills6', 2, 'checked');
  await foldersPage.addFolder('Bills7');
  await foldersPage.notificationShows('Bills7 created');
  await foldersPage.parentFolderHasBeenAdded('Bills7', 3, 'checked');
  await foldersPage.addFolder('Bills8');
  await foldersPage.notificationShows('Folder limit reached. Please upgrade to a paid plan to add more folders')
  await foldersPage.cancelFolderCreation();
  await foldersPage.deleteParentFolder('Bills5');
  await foldersPage.deleteParentFolder('Bills6');
  await foldersPage.deleteParentFolder('Bills7');
});
