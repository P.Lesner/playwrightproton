const { expect } = require('@playwright/test');

exports.LoginPage = class LoginPage {
    /**
     * @param {import('playwright').Page} page 
     */
    constructor(page) {
        this.page = page;
        this.usernameField = page.locator('#username');
        this.passwordField = page.locator('#password');
        this.submitButton = page.locator('button[type="submit"]');
    }
    async open() {
        await this.page.goto('https://account.protonmail.com/login');
    }
    async login(username, password) {
        await expect(this.usernameField).toBeVisible();
        await expect(this.passwordField).toBeVisible();
        await expect(this.submitButton).toBeVisible();
        await this.usernameField.fill(username);
        await this.passwordField.fill(password);
        await this.submitButton.click();
        await expect(this.page).toHaveURL("https://mail.protonmail.com/u/0/inbox", { timeout: 10000 });
    }
}