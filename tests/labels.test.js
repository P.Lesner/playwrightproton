const { test, expect } = require('@playwright/test');
const { LoginPage } = require ('../page/login.page');
const { TopBar } = require ('../page/top_bar.page');
const { LabelsPage } = require ('../page/labels.page');

test.beforeEach(async ({ page }) => {
  const loginPage = new LoginPage(page);
  await loginPage.open();
  await loginPage.login(process.env.USERNAME, process.env.PASSWORD);
});

test.afterEach(async ({ page }) => {
  const topBar = new TopBar(page);
  await topBar.openUserdropdown();
  await topBar.logout();
});

test('Create yellow label', async ({ page }) => {
  const labelsPage = new LabelsPage(page);
  await labelsPage.open();
  await labelsPage.addLabel('Important', 'rgb(230, 192, 76)');
  await labelsPage.notificationShows('Important created');
  await labelsPage.labelHasBeenAdded('Important', 1, 'rgb(230, 192, 76)');
  await labelsPage.removeLabel('Important');
  await labelsPage.notificationShows('Important removed');
  await labelsPage.labelHasBeenRemoved('Important');
});

test('Create label with already taken name', async ({ page }) => {
  const labelsPage = new LabelsPage(page);
  await labelsPage.open();
  await labelsPage.addLabel('Important2', 'rgb(230, 192, 76)');
  await labelsPage.notificationShows('Important2 created');
  await labelsPage.labelHasBeenAdded('Important2', 1, 'rgb(230, 192, 76)');
  await labelsPage.addLabel('Important2', 'rgb(230, 192, 76)');
  await labelsPage.notificationShows('A label or folder with this name already exists');
  await labelsPage.cancelLabelCreation();
  await labelsPage.removeLabel('Important2');
  await labelsPage.notificationShows('Important2 removed');
  await labelsPage.labelHasBeenRemoved('Important2');
});

test('Update label', async ({ page }) => {
  const labelsPage = new LabelsPage(page);
  await labelsPage.open();
  await labelsPage.addLabel('Important3', 'rgb(230, 192, 76)');
  await labelsPage.notificationShows('Important3 created');
  await labelsPage.labelHasBeenAdded('Important3', 1, 'rgb(230, 192, 76)');
  await labelsPage.openLabelsEditor('Important3');
  await labelsPage.updateLabel('Important100', 'rgb(94, 199, 183)');
  await labelsPage.notificationShows('Important100 updated');
  await labelsPage.labelHasBeenUpdated('Important100', 1, 'rgb(94, 199, 183)');
  await labelsPage.removeLabel('Important100');
  await labelsPage.notificationShows('Important100 removed');
  await labelsPage.labelHasBeenRemoved('Important100');
});

test("User can't update label to already taken name", async ({ page }) => {
  const labelsPage = new LabelsPage(page);
  await labelsPage.open();
  await labelsPage.addLabel('Important3', 'rgb(230, 192, 76)');
  await labelsPage.notificationShows('Important3 created');
  await labelsPage.labelHasBeenAdded('Important3', 1, 'rgb(230, 192, 76)');
  await labelsPage.addLabel('Important4', 'rgb(94, 199, 183)');
  await labelsPage.notificationShows('Important4 created');
  await labelsPage.labelHasBeenAdded('Important4', 1, 'rgb(94, 199, 183)');
  await labelsPage.openLabelsEditor('Important3');
  await labelsPage.updateLabel('Important4', 'rgb(94, 199, 183)');
  await labelsPage.notificationShows('A label or folder with this name already exists');
  await labelsPage.cancelLabelUpdate();
  await labelsPage.labelHasntBeenUpdated('Important3', 1, 'rgb(230, 192, 76)');
  await labelsPage.removeLabel('Important3');
  await labelsPage.removeLabel('Important4');
});

