# playwrightproton

## Description

Repository contains automated tests of labels and folders in ProtonMail. Chosen automation framework is [Playwright](https://playwright.dev/).

## Test cases

Automated test cases cover only crucial part like adding, deleting folders and subfolders as well as adding, updating and deleting labels.

## Project structure
Chosen design pattern is Page Object Model.
`test` directory contains test suites divided by objects.
`pages` directory contains page objects with methods and selectors.

## How to start
* install playwright by following [official tutorial](https://playwright.dev/docs/intro)

* create .env file and put there username and password to test account. You can copy using this command:
    ```commandline
    cp .env.example .env
    ```

* instal dotenv to allow playwright to use process.env.
    ```commandline
    npm install dotenv --save
    ```
    Read more [here](https://www.npmjs.com/package/dotenv).


