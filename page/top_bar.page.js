const { expect } = require('@playwright/test');

exports.TopBar = class TopBAr {
    /**
     * @param {import('playwright').Page} page 
     */
    constructor(page) {
        this.page = page;
        this.userdropdown = page.locator('button[data-testid="heading:userdropdown"]');
        this.logoutbutton = page.locator('button[data-testid="userdropdown:button:logout"]');
    }
    async openUserdropdown() {
        await expect(this.userdropdown).toBeVisible();
        await this.userdropdown.click();
    }
    async logout(username, password) {
        await expect(this.logoutbutton).toBeVisible();
        await this.logoutbutton.click();
        await expect(this.page).toHaveURL("https://account.protonmail.com/login", { timeout: 10000 });
    }
}