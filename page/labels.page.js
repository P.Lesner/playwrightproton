const { expect } = require('@playwright/test');

exports.LabelsPage = class LabelsPage {
    /**
     * @param {import('playwright').Page} page 
     */
     constructor(page) {
        this.page = page;
        this.addLabelButton = page.locator('text=Add label');
        this.labelNameField = page.locator('#accountName');
        this.labelColorDropdown = page.locator('//form//button[@data-testid="dropdown-button"]');
        this.cancelLabelCreationBtn = page.locator('text=Cancel');
        this.saveLabelBtn = page.locator('text=Save');
        this.deleteLabelBtn = page.locator(`button:has-text("Delete")`);
        this.titleConfirmationPrompt = page.locator(`#modalTitle`);
        this.confirmDeletionBtn = page.locator(`button[type="submit"]`);
        this.cancelLabelCreationBtn = page.locator('text=Cancel');
    }
    async open() {
        await this.page.goto('https://account.protonmail.com/u/0/mail/folders-labels');
    }
    async addLabel(name, color) {
        await expect(this.addLabelButton).toBeVisible();
        await this.addLabelButton.click();
        await expect(this.labelNameField).toBeVisible();
        await this.labelNameField.fill(name);
        if (color !== "") {
            await expect(this.labelColorDropdown).toBeVisible();
            await this.labelColorDropdown.click();
            this.colorItem = this.page.locator(`li[style="color: ${color};"] > input`);
            await expect(this.colorItem).toBeVisible();
            await this.colorItem.click();
            this.chosenColorItem = this.page.locator(`//li[@style="color: ${color};"][@class="color-selector-item is-selected"]`);
            await expect(this.chosenColorItem).toHaveCount(1);
        }
        await expect(this.saveLabelBtn).toBeVisible();
        await this.saveLabelBtn.click();
    }
    async openLabelsEditor(name) {
        this.labelsEdit = this.page.locator(`//span[text()="${name}"]/parent::div/parent::td/parent::tr//button[@data-test-id="folders/labels:item-edit"]`);
        await expect(this.labelsEdit).toBeVisible();
        await this.labelsEdit.click();
    }
    async updateLabel(name, color) {
        await expect(this.labelNameField).toBeVisible();
        await this.labelNameField.clea
        await this.labelNameField.fill("");
        await this.labelNameField.fill(name);
        if (color !== "") {
            await expect(this.labelColorDropdown).toBeVisible();
            await this.labelColorDropdown.click();
            this.colorItem = this.page.locator(`li[style="color: ${color};"] > input`);
            await expect(this.colorItem).toBeVisible();
            await this.colorItem.click();
            this.chosenColorItem = this.page.locator(`//li[@style="color: ${color};"][@class="color-selector-item is-selected"]`);
            await expect(this.chosenColorItem).toHaveCount(1);
        }
        await expect(this.saveLabelBtn).toBeVisible();
        await this.saveLabelBtn.click();
    }
    async labelHasBeenUpdated(name, location, color) {
        this.labelHasBeenAdded(name, location, color);
    }
    async labelHasntBeenUpdated(name, location, color) {
        this.labelHasBeenAdded(name, location, color);
    }
    async labelHasBeenAdded(name, location, color) {
        this.labelAdded = this.page.locator(`//tbody//tr[${location}]//span[text()="${name}"]`);
        await expect(this.labelAdded).toBeVisible();
        const labelAddedColor = this.page.locator(`//tbody//tr[${location}]//span[text()="${name}"]/parent::div/*[name()='svg']`);
        await expect(labelAddedColor).toHaveAttribute('style', `fill: ${color};`);
    }
    async removeLabel(name) {
        this.labelsDropdownMenu = this.page.locator(`//span[text()="${name}"]/parent::div/parent::td/parent::tr//button[@data-testid="dropdown-button"]`);
        await this.labelsDropdownMenu.click();
        await expect(this.deleteLabelBtn).toBeVisible();
        await this.deleteLabelBtn.click();
        await expect(this.titleConfirmationPrompt).toHaveText(`Delete ${name} label`);
        await expect(this.confirmDeletionBtn).toBeVisible();
        await this.confirmDeletionBtn.click();
    }
    async labelHasBeenRemoved(name) {
        await expect(this.confirmDeletionBtn).not.toBeVisible();
        this.labelsDropdownMenu = this.page.locator(`//span[text()="${name}"]/parent::div/parent::td/parent::tr//button[@data-testid="dropdown-button"]`);
        await expect(this.labelsDropdownMenu).not.toBeVisible();
    }
    async notificationShows(text) {
        this.notificationLabel = this.page.locator(`//div[contains(text(),"${text}") and @aria-atomic="true"]`);
        await expect(this.notificationLabel).toHaveText(text);
        await expect(this.notificationLabel).not.toBeVisible();
    }
    async cancelLabelCreation() {
        await expect(this.cancelLabelCreationBtn).toBeVisible();
        await this.cancelLabelCreationBtn.click();
        await expect(this.cancelLabelCreationBtn).not.toBeVisible();
    }
    async cancelLabelUpdate() {
        this.cancelLabelCreation();
    }
}