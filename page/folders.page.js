const { expect } = require('@playwright/test');

exports.FoldersPage = class FoldersPage {
    /**
     * @param {import('playwright').Page} page 
     */
    constructor(page) {
        this.page = page;
        this.addfolderButton = page.locator('text=Add folder');
        this.folderNameField = page.locator('#accountName');
        this.locationSelect = page.locator('#parentID');
        this.notificationToggle = page.locator('#notification');
        this.notificationToggleLabel = page.locator('.toggle-label[for="notification"]');
        this.saveNewFolderBtn = page.locator('button[type="submit"]');
        this.deleteFolderBtn = page.locator(`button:has-text("Delete")`);
        this.titleConfirmationPrompt = page.locator(`#modalTitle`);
        this.confirmDeletionBtn = page.locator(`button[type="submit"]`);
        this.parentFolderSelect = page.locator(`#parentID`);
        this.cancelFolderCreationBtn = page.locator('text=Cancel');
    }
    async open() {
        await this.page.goto('https://account.protonmail.com/u/0/mail/folders-labels');
    }
    async addFolder(name, notification) {
        await expect(this.addfolderButton).toBeVisible();
        await this.addfolderButton.click();
        await expect(this.folderNameField).toBeVisible();
        await this.folderNameField.fill(name);
        await expect(this.saveNewFolderBtn).toBeVisible();
        await this.saveNewFolderBtn.click();
    }
    async parentFolderHasBeenAdded(name, orderNo, notification) {
        this.addedFolder = this.page.locator(`span[title="${name}"]`);
        await expect(this.addedFolder).toBeVisible();
        this.addedFolder = this.page.locator(`span:has-text("${name}")`).first();
        await expect(this.addedFolder).toBeVisible();
        await expect(this.addedFolder).toHaveText(name);
        this.notificationsOn = this.page.locator(`ul.treeview-container > li:nth-child(${orderNo}) > div > div > div > span > label.toggle-label--${notification}`);
        await expect(this.notificationsOn).toBeVisible();
    }
    async deleteParentFolder(name) {
        this.addedFolderActionMenu = this.page.locator(`//li[@title="${name}"]//button[@data-test-id="dropdown:open"]`).first();
        await expect(this.addedFolderActionMenu).toBeVisible();
        await this.addedFolderActionMenu.click();
        await expect(this.deleteFolderBtn).toBeVisible();
        await this.deleteFolderBtn.click();
        await expect(this.titleConfirmationPrompt).toHaveText(`Delete ${name} folder`);
        await expect(this.confirmDeletionBtn).toBeVisible();
        await this.confirmDeletionBtn.click();
        await expect(this.addedFolderActionMenu).not.toBeVisible();
    }
    async addSubFolder(name, location, notification) {
        await expect(this.addfolderButton).toBeVisible();
        await this.addfolderButton.click();
        await expect(this.folderNameField).toBeVisible();
        await this.folderNameField.fill(name);
        const option = await this.page.$(`//option[text()="${location}"]`);
        await this.page.selectOption('#parentID', option);
        await expect(this.saveNewFolderBtn).toBeVisible();
        await this.saveNewFolderBtn.click();
    }
    async subFolderHasBeenAdded(name, location, orderNo, notification) {
        this.addedSubFolder = this.page.locator(`//li[@title="${location}"]//li[${orderNo}]//span[@title="${name}"]`);
        await expect(this.addedSubFolder).toBeVisible();
        if (notification === "on") {
            this.notificationsOn = this.page.locator(`//li[@title="${location}"]//li[${orderNo}]//label[@class="toggle-label toggle-label--checked"]`);
            await expect(this.notificationsOn).toBeVisible();
          } else {
            this.notificationsOn = this.page.locator(`//li[@title="${location}"]//li[${orderNo}]//label[@class="toggle-label"]`);
            await expect(this.notificationsOn).toBeVisible();
          }
    }
    async deleteSubFolder(name, location) {
        this.addedSubFolderActionMenu = this.page.locator(`//li[@title="${location}"]//li[@title="${location}/${name}"]//button[@data-test-id="dropdown:open"]`);
        await expect(this.addedSubFolderActionMenu).toBeVisible();
        await this.addedSubFolderActionMenu.click();
        await expect(this.deleteFolderBtn).toBeVisible();
        await this.deleteFolderBtn.click();
        await expect(this.titleConfirmationPrompt).toHaveText(`Delete ${name} folder`);
        await expect(this.confirmDeletionBtn).toBeVisible();
        await this.confirmDeletionBtn.click();
        await expect(this.confirmDeletionBtn).not.toBeVisible();
        await expect(this.addedSubFolderActionMenu).not.toBeVisible();
    }
    async subfolderHasBeenRemoved(name, location) {
        this.addedSubFolderActionMenu = this.page.locator(`//li[@title="${location}"]//li[@title="${location}/${name}"]//button[@data-test-id="dropdown:open"]`);
        await expect(this.addedSubFolderActionMenu).not.toBeVisible();
    }
    async notificationShows(text) {
        this.notificationFolder = this.page.locator(`//div[contains(text(),"${text}") and @aria-atomic="true"]`);
        await expect(this.notificationFolder).toHaveText(text);
        await expect(this.notificationFolder).not.toBeVisible();
    }
    async cancelFolderCreation() {
        await expect(this.cancelFolderCreationBtn).toBeVisible();
        await this.cancelFolderCreationBtn.click();
        await expect(this.cancelFolderCreationBtn).not.toBeVisible();
    }
    async folderHasBeenRemoved(name) {
        this.addedFolderActionMenu = this.page.locator(`//li[@title="${name}"]//button[@data-test-id="dropdown:open"]`).first();
        await expect(this.addedFolderActionMenu).not.toBeVisible();
    }
}